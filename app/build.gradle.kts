
plugins {
	application
	eclipse
}

repositories {
	mavenCentral()
	
	maven("https://gitlab.com/api/v4/projects/11826202/packages/maven")
}

dependencies {
	testImplementation("junit:junit:4.13.2")
	
	implementation("net.worldsynth:opennbt:1.4.1")
}

application {
	mainClass.set("com.booleanbyte.nbttosnbtutil.App")
}